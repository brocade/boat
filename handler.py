# -*- coding:utf-8 -*-
# created:     Mon Aug 31 18:25:06 2015
# filename:    handler.py
# author:      juntao liu
# email:       jinuljt@gmail.com
# descritpion:

import os
import json
import urlparse
import urllib
import datetime
import time
import uuid

from jinja2 import Environment, FileSystemLoader, TemplateNotFound
from PIL import Image
from wechat_sdk import WechatBasic

import tornado.web
from tornado.httpclient import AsyncHTTPClient

from wechat import (wxauth, get_wechat_basic,
                    WECHAT_TOKEN, WECHAT_APP_ID, WECHAT_APP_SECRET)
from forms import WechatAuthForm, ShareForm
from emsg import ERROR_PARAMS
from short_url import decode_url, encode_url
from utils import square_image_box, image_hash_name, save_image, message_escape

CACHE_PREFIX="hifan_boat:"
CACHE_TIMEOUT=10*60

class TemplateRendering:
    """
    A simple class to hold methods for rendering templates.
    """
    def render_template(self, template_name, **kwargs):
        template_dirs = []
        if self.settings.get('template_path', ''):
            template_dirs.append(self.settings["template_path"])

        env = Environment(loader=FileSystemLoader(template_dirs))

        try:
            template = env.get_template(template_name)
        except TemplateNotFound:
            raise TemplateNotFound(template_name)
        content = template.render(kwargs)
        return content

def _prefix(key):
    return CACHE_PREFIX + key

class BaseHandler(tornado.web.RequestHandler, TemplateRendering):
    @property
    def db(self):
        return self.application.db

    @property
    def redis(self):
        return self.application.redis

    @property
    def wechat(self):
        return get_wechat_basic()

    def render_jinja(self, template_name, **kwargs):
        kwargs.update({
            'settings': self.settings,
            'STATIC_URL': self.settings.get('static_url_prefix', '/static/'),
            'request': self.request,
            'xsrf_token': self.xsrf_token,
            'xsrf_form_html': self.xsrf_form_html,
            })
        content = self.render_template(template_name, **kwargs)
        self.write(content)

    def get_journey_id(self, openid):
        '''检查上传图片session是否存在
        '''
        journey_id = self.redis.get(_prefix("JOURNEY:%s"% openid))
        if journey_id:
            return int(journey_id)
        return None

    def set_journey_id(self, openid, journey_id):
        self.redis.setex(_prefix("JOURNEY:%s"% openid),
                         journey_id,
                         CACHE_TIMEOUT)


class MainHandler(BaseHandler):
    def error(self, emsg, status=400):
        self.set_status(status)
        self.write(json.dumps(emsg))

    def check_signature(self):
        form = WechatAuthForm(self.request.query_arguments)
        if (form.validate() and
            self.wechat.check_signature(form.data['signature'],
                                   form.data['timestamp'],
                                   form.data['nonce'])):
            return True
        return False

    def get(self):
        if  not self.check_signature():
            return self.error(ERROR_PARAMS)
        self.write(self.get_argument('echostr'))

    def post(self):
        if not self.check_signature():
            return self.error(ERROR_PARAMS)

        wechat = self.wechat
        wechat.parse_data(self.request.body)
        message = wechat.get_message()

        weixin_handler = getattr(self, "handle_weixin_" + message.type, None)
        if weixin_handler:
            weixin_handler(wechat, message)


    def handle_weixin_subscribe(self, wechat, message):
        response = wechat.response_text(u'感谢关注嗨帆，嗨帆淀山湖体验活动正在进行，' +
                                             u'点击下方菜单“我要出航”即可参与。')
        self.write(response)

    def handle_weixin_image(self, wechat, message):
        if not message.picurl:
            return self.finish()

        # check session
        open_id = message.source
        journey_id = self.get_journey_id(message.source)
        response = wechat.response_text(u'感谢您的参与。上传会话已结束。' +
                                         u'您可以再次点击下方菜单“分享航程”修改您上传的内容。')

        # 检查上传session是否超时
        if not journey_id:
            return self.write(response)

        row = self.db.get('''SELECT journey.id, journey.photos
        FROM journey, user
        WHERE journey.id=%s AND journey.user_id=user.id AND user.openid=%s''',
                          journey_id, open_id)
        # 检查行程是否存在
        if not row:
            return self.write(response)

        self.finish()

        self.process_image(journey_id, open_id, message.picurl)



    def process_image(self, journey_id, openid, picurl):
        def handle_request(response):
            if response.error:
                print "Error:", response.error
                return
            origin_im = Image.open(response.buffer).convert("RGB")
            square_im = origin_im.crop(square_image_box(origin_im.height,
                                                        origin_im.width))
            square_im = square_im.resize((self.settings['image_square_width'],
                                          self.settings['image_square_height']))
            hash_name = image_hash_name(square_im)
            origin_fname = os.path.join(self.settings['image_upload_dir'],
                                        hash_name + "_o.jpeg")
            square_fname = os.path.join(self.settings['image_upload_dir'],
                                        hash_name + "_s.jpeg")
            save_image(origin_im, origin_fname)
            save_image(square_im, square_fname)

            # save to data
            self.db.execute("START TRANSACTION")
            try:
                photos = []
                row = self.db.get("SELECT photos FROM  journey WHERE id=%s FOR UPDATE",
                                  journey_id)
                photos_string = row['photos']
                if photos_string:
                    photos = json.loads(photos_string)
                photos.append(hash_name)
                self.db.execute("UPDATE journey SET photos=%s WHERE id=%s",
                                    json.dumps(photos[-9:]),
                                    journey_id)
            except Exception, e:
                print e
                self.db.execute("ROLLBACK")
            else:
                self.db.execute("COMMIT")
            count = min(len(photos), 9)
            msg = (u'已收到您发送的照片，目前一共有%d张照片\n'% count +
                   u'可继续上传照片，最近上传的9张照片将出现在您的分享页面中；\n' +
                   u'点击下方的”分享网页“链接打开分享页，' +
                   u'分享到朋友圈可获优惠券哦！\n' +
                   u'分享网页→ http://boat.msglife.com/journey/%s\n'%
                   encode_url(journey_id))
            self.wechat.send_text_message(openid, msg)


        http_client = AsyncHTTPClient()
        http_client.fetch(picurl, handle_request)






class ShareHandler(BaseHandler):
    def get(self):
        code = self.get_argument('code', None)
        if not code:
            callback_url = urlparse.urlunsplit([self.request.protocol,
                                                self.request.host,
                                                self.request.uri,
                                                "",
                                                ""])
            redirect_url = wxauth.get_connect_url(urllib.quote(callback_url,
                                                               safe=""))
            return self.redirect(redirect_url)
        resp = wxauth.get_access_token(code)
        userinfo = wxauth.get_userinfo(resp.access_token)

        # 创建或更新微信用户信息
        now = datetime.datetime.now()
        row = self.db.get('''SELECT * FROM user WHERE openid=%s''', userinfo.openid)
        if row:
            self.db.execute('''UPDATE user SET nickname=%s, province=%s,
            headimgurl=%s, modify_at=%s
            WHERE openid=%s''',
                            userinfo.nickname,
                            userinfo.province,
                            userinfo.headimgurl,
                            userinfo.openid,
                            now)
        else:
            self.db.execute('''INSERT INTO user (nickname,province,
            headimgurl,openid,create_at,modify_at) VALUE(%s,%s,%s,%s,%s,%s)''',
                            userinfo.nickname,
                            userinfo.province,
                            userinfo.headimgurl,
                            userinfo.openid,
                            now,
                            now
                            )
        shares = {}
        for row in  self.db.query("""SELECT share.id, share.date, boat.name
        FROM share, boat
        WHERE share.boat_id = boat.id AND share.enable=True
        ORDER BY share.date DESC
        """):
            share = shares.setdefault(row['date'].isoformat(), {'boat': []})
            share['boat'].append({"id": row['id'],
                                  "name": row['name'],})
        l = []
        for k, v in shares.items():
            v['date'] = k
            l.append(v)
        self.render_jinja("input.html",
                          shares=json.dumps(l),
                          openid = userinfo.openid)

    def post(self):
        # 如果分享的日期（用户相关）已经存在，清空照片，否创建
        form = ShareForm(self.request.body_arguments)
        if form.validate():
            openid = form.data['openid']
            user  = self.db.get("SELECT id FROM user where openid=%s",
                                openid)
            share  = self.db.get("SELECT id, date FROM share where id=%s",
                                 form.data['share_id'])
            now = datetime.datetime.now()
            if share and user:
                journey = self.db.get('''SELECT id FROM journey
                WHERE user_id=%s AND share_id=%s''',
                                      user['id'], share['id'])
                date = share['date']
                if journey:
                    journey_id = journey['id']
                    # journey 存在，清空，修改message
                    self.db.execute(''' UPDATE journey
                    SET message=%s, photos="", modify_at=%s
                    WHERE id=%s''',
                                    form.data['comment'],
                                    now,
                                    journey_id)
                else:
                    journey_id = self.db.execute_lastrowid('''INSERT INTO journey(
                    user_id, share_id, date, message, photos, create_at, modify_at)
                    VALUES(%s, %s, %s, %s, %s, %s, %s)''',
                                    user['id'],
                                    share['id'],
                                    date,
                                    form.data['comment'],
                                    "",
                                    now,
                                    now)
                self.set_journey_id(openid, journey_id)
            self.wechat.send_text_message(openid,
                                          u"您已选择%s年%s月%s日的活动\n"% (date.year,
                                                                            date.month,
                                                                            date.day) +
                                          u"感谢参与嗨帆活动，请在5分钟内通过微信发送照片到本公众号生成分享页面。")
        else:
            print form.errors
        self.write("close me")


class JourneyHandler(BaseHandler):
    def get(self, journey_id):
        try:
            _journey_id = decode_url(journey_id)
        except Exception, e:
            # TODO log
            return self.write("journey not exists")
        row = self.db.get('''SELECT user.nickname, user.headimgurl, share.date,
        share.weather, journey.message, journey.photos, boat.name as boat_name,
        boat.icon as boat_icon, boat.pic as boat_pic,
        boat.description as boat_desc, captain.name as captain,
        dock.longitude, dock.latitude, dock.pic as dock_pic,
        dock.description as dock_desc
        FROM journey, user, share, boat, captain, dock
        WHERE journey.id=%s AND journey.user_id=user.id AND
        journey.share_id=share.id AND share.boat_id = boat.id AND
        share.captain_id = captain.id AND share.dock_id = dock.id''',
                      _journey_id)

        if not row:
            return self.write("journey not exists")

        squares = []
        origins = []
        if row['photos']:
            photos = json.loads(row['photos'])
            for photo in photos:
                url = (self.settings['image_upload_uri'] + photo)
                squares.append(url + "_s.jpeg")
                origins.append(url + "_o.jpeg")
        row['squares'] = json.dumps(squares)
        row['origins'] = json.dumps(origins)
        row['dock_pic'] = (self.settings['image_upload_uri'] + row['dock_pic'])
        row['boat_pic'] = (self.settings['image_upload_uri'] + row['boat_pic'])

        d = {'timestamp': str(int(time.time())),
             'noncestr': uuid.uuid4().get_hex(),
             'url': urlparse.urlunsplit([self.request.protocol,
                                         self.request.host,
                                         self.request.uri,
                                         "",
                                         ""])}
        d['signature'] = self.wechat.generate_jsapi_signature(**d)
        d['app_id'] = WECHAT_APP_ID
        d.update(row)
        d['message'] = message_escape(row['message'])
        self.render_jinja("index.html", **d)


class JourneyClearHandler(BaseHandler):
    def get(self):
        code = self.get_argument('code', None)
        if not code:
            callback_url = urlparse.urlunsplit([self.request.protocol,
                                                self.request.host,
                                                self.request.uri,
                                                "",
                                                ""])
            redirect_url = wxauth.get_connect_url(urllib.quote(callback_url,
                                                               safe=""))
            return self.redirect(redirect_url)
        resp = wxauth.get_access_token(code)
        userinfo = wxauth.get_userinfo(resp.access_token)

        journey_id = self.get_journey_id(userinfo.openid)
        if journey_id:
            self.db.execute('''UPDATE journey, user SET journey.photos=""
            WHERE journey.id=%s AND journey.user_id=user.id AND user.openid=%s''',
                        journey_id,
                        userinfo.openid)
        self.write('close me')
