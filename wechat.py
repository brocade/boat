#-*- coding:utf-8 -*-
# created:     Mon Aug 31 18:25:31 2015
# filename:    wechat.py
# author:      juntao liu
# email:       jinuljt@gmail.com
# descritpion:

import json

import redis

from wechat_sdk import WechatBasic
from wechat_web_auth import Wechat


from manager.preference import (REDIS_HOST,  REDIS_PORT, REDIS_DB)



WECHAT_TOKEN="71eda83f16ff488185a1fa8aa24ea5c4"
WECHAT_APP_ID="wx41eefc4172523b69"
WECHAT_APP_SECRET="c811c4ab090712bdb30afe35ae0e5ec8"
WECHAT_INFO_TIMEOUT = 1*60*60
wxauth = Wechat(WECHAT_APP_ID, WECHAT_APP_SECRET) #web auth

redis = redis.Redis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)

def get_wechat_basic(override=False):
    info = None
    key = "WECHAT:INFO"
    if not override:
        info = redis.get(key)

    if not info:
        wxbasic = WechatBasic(token=WECHAT_TOKEN,
                              appid=WECHAT_APP_ID,
                              appsecret=WECHAT_APP_SECRET)
        jsapi_ticket = wxbasic.get_jsapi_ticket()
        access_token = wxbasic.get_access_token()
        info = {
            'jsapi_ticket': jsapi_ticket['jsapi_ticket'],
            'jsapi_ticket_expires_at': jsapi_ticket['jsapi_ticket_expires_at'],
            'access_token': access_token['access_token'],
            'access_token_expires_at': access_token['access_token_expires_at']
            }
        redis.setex(key, json.dumps(info), WECHAT_INFO_TIMEOUT)
    else:
        info =  json.loads(info)
    return WechatBasic(
        token=WECHAT_TOKEN,
        appid=WECHAT_APP_ID,
        appsecret=WECHAT_APP_SECRET,
        access_token=info['access_token'],
        access_token_expires_at=info['access_token_expires_at'],
        jsapi_ticket=info['jsapi_ticket'],
        jsapi_ticket_expires_at=info['jsapi_ticket_expires_at'],
        )

# 给用户发消息
#wechat.grant_token()
#wechat.send_text_message("openid", "message")
