#-*- coding:utf-8 -*-
# created:     Fri Aug 28 16:41:20 2015
# filename:    forms.py
# author:      juntao liu
# email:       jinuljt@gmail.com
# descritpion:


from wtforms_tornado import Form
from wtforms.fields import StringField, IntegerField
from wtforms.validators import InputRequired, Length

class WechatAuthForm(Form):
    signature = StringField(validators=[InputRequired()])
    timestamp  = StringField(validators=[InputRequired()])
    nonce = StringField(validators=[InputRequired()])


class ShareForm(Form):
    share_id = IntegerField(validators=[InputRequired()])
    openid = StringField(validators=[InputRequired()])
    comment = StringField(validators=[InputRequired(), Length(max=140)])
