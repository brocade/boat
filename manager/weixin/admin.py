#-*- coding:utf-8 -*-
# created:     Tue Sep  1 10:36:16 2015
# filename:    admin.py
# author:      juntao liu
# email:       jinuljt@gmail.com
# descritpion:

from django.contrib import admin
from . import models

for name in dir(models):
    try:
        m = getattr(models, name)
        if m._meta.app_label == 'weixin':
            if globals().get(m.__name__ + "Admin"):
                admin.site.register(m, globals()[m.__name__ + 'Admin'])
            else:
                try:
                    admin_class = type(
                        "%sAdmin"%m.__name__,
                        (admin.ModelAdmin,),
                        {"list_display": [f.name for f in m._meta.fields]})
                    admin.site.register(m, admin_class)
                except Exception, e:
                    pass
    except Exception, e:
        pass
