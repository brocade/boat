#-*- coding:utf-8 -*-
# created:     Tue Sep  1 09:44:42 2015
# filename:    models.py
# author:      juntao liu
# email:       jinuljt@gmail.com
# descritpion:

from django.db.models import (Model,
                              CharField, DateTimeField, DateField,
                              BooleanField, TextField, URLField, ImageField,
                              ForeignKey)


class BaseModel(Model):
    class Meta:
        abstract = True

    create_at = DateTimeField(auto_now_add=True)
    modify_at = DateTimeField(auto_now=True)


class Share(BaseModel):
    class Meta:
        db_table = 'share'
        verbose_name = "航程分享日期"
        unique_together = (('date', 'boat'),)

    def __unicode__(self):
        return "|".join([self.date.isoformat(), self.boat.name, self.captain.name])

    date = DateField(verbose_name="日期")
    weather = CharField(max_length=100)
    boat = ForeignKey('Boat')
    captain  = ForeignKey('Captain')
    dock = ForeignKey('Dock')
    description = TextField(null=True, blank=True)
    enable = BooleanField(default=True, verbose_name="是否可分享")

class Boat(BaseModel):
    class Meta:
        db_table = 'boat'

    def __unicode__(self):
        return self.name

    name = CharField(max_length=50)
    icon = ImageField(upload_to="%Y/%m/%d")
    pic = ImageField(upload_to="%Y/%m/%d")
    description = TextField()


class Captain(BaseModel):
    class Meta:
        db_table = 'captain'

    def __unicode__(self):
        return self.name

    name = CharField(max_length=50)
    mobile = CharField(max_length=15)
    description = TextField()


class Dock(BaseModel):
    class Meta:
        db_table = 'dock'

    def __unicode__(self):
        return self.name

    name = CharField(max_length=50)
    longitude = CharField(max_length=20, verbose_name="经度")
    latitude  = CharField(max_length=20, verbose_name="纬度")
    pic = ImageField(upload_to="%Y/%m/%d")
    description = TextField()


class User(BaseModel):
    class Meta:
        db_table = "user"

    def __unicode__(self):
        return self.nickname

    openid = CharField(max_length=100, unique=True)
    nickname = CharField(max_length=50)
    province = CharField(max_length=10)
    headimgurl = URLField()


class Journey(BaseModel):
    class Meta:
        db_table = "journey"
        unique_together = (('user', 'share'),)

    user = ForeignKey(User)
    share = ForeignKey(Share)
    date = DateField()
    message = CharField(max_length=200)
    photos = TextField(default="", blank=True)
