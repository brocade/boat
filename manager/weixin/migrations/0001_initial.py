# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ShareDate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('create_at', models.DateTimeField(auto_now_add=True)),
                ('modify_at', models.DateTimeField(auto_now=True)),
                ('date', models.DateField(verbose_name=b'\xe8\x88\xaa\xe7\xa8\x8b\xe5\x88\x86\xe4\xba\xab\xe6\x97\xa5\xe6\x9c\x9f')),
                ('description', models.TextField(default=b'')),
                ('enable', models.BooleanField(default=True, verbose_name=b'\xe6\x98\xaf\xe5\x90\xa6\xe5\x8f\xaf\xe4\xbb\xa5\xe5\x88\x86\xe4\xba\xab')),
            ],
            options={
                'db_table': 'share_date',
            },
        ),
    ]
