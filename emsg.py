#-*- coding:utf-8 -*-
# created:     Fri Aug 28 17:27:09 2015
# filename:    emsg.py
# author:      juntao liu
# email:       jinuljt@gmail.com
# descritpion:



SUCCESS = {"msg": "success",
           "code": 0}

ERROR_PARAMS = {"msg": "params error",
                "code": 1000}
ERROR_INTERNAL = {"msg": "internal server error",
                  "code": 1001}
