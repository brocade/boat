# -*- coding:utf-8 -*-
# created:     Fri Sep 11 16:52:24 2015
# filename:    utils.py
# author:      juntao liu
# email:       jinuljt@gmail.com
# descritpion:

import os
import errno
from hashlib import md5


def square_image_box(height, width):
    if height > width:
        fill_height = (height - width)/2
        return (0, fill_height, width, height - fill_height)

    fill_width = (width - height)/2
    return (fill_width, 0, width - fill_width, height)


def image_hash_name(im):
    digest = md5(im.tobytes()).hexdigest()
    return os.path.join(digest[:2], digest[2:4], digest[4:])


def save_image(im, fname):
    path = os.path.dirname(fname)
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            print exc
    im.save(fname, "JPEG")


def message_escape(msg):
    return msg.replace('\n', '').replace('\r', '')
