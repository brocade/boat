#-*- coding:utf-8 -*-
# created:     Fri Aug 28 15:25:02 2015
# filename:    app.py
# author:      juntao liu
# email:       jinuljt@gmail.com
# descritpion:

import os

import torndb
import redis

import tornado.ioloop
import tornado.web
from tornado.options import define, options

from wechat import get_wechat_basic
from handler import (MainHandler, ShareHandler,
                     JourneyHandler, JourneyClearHandler)
from manager.preference import (DEBUG,
                                DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS,
                                REDIS_HOST,  REDIS_PORT, REDIS_DB,
                                UPLOAD_DIR, UPLOAD_URI)




define("host", default="127.0.0.1", help="listen host")
define("port", default=8001, help="listen port")
options.parse_command_line()


def create_menu():
    wxbasic = get_wechat_basic(override=True)
    wxbasic.create_menu({
        'button':[
        {
            'type': 'view',
            'name': '我要出航',
            'url': 'http://wap.koudaitong.com/v2/feature/fbenfaj6'
        },
        {
            'type': 'view',
            'name': '分享航程',
            'url': 'http://boat.msglife.com/share',
        }
        ]})


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/", MainHandler),
            (r"/share$", ShareHandler),
            (r"/journey/clear", JourneyClearHandler),
            (r"/journey/(?P<journey_id>\w+)", JourneyHandler),
            ]

        settings = dict(
            debug=DEBUG,
            template_path=os.path.join(os.path.dirname(__file__),
                                       "templates"),
            image_upload_dir=UPLOAD_DIR,
            image_square_height=512,
            image_square_width=512,
            image_upload_uri=UPLOAD_URI,
            )
        super(Application, self).__init__(handlers, **settings)
        self.db = torndb.Connection(host="%s:%s"% (DB_HOST, DB_PORT),
                                    database=DB_NAME,
                                    user=DB_USER,
                                    password=DB_PASS,
                                    charset="utf8mb4")
        self.redis = redis.Redis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)


if __name__ == "__main__":
    create_menu()
    application = Application()
    application.listen(options.port, options.host)
    print "listen: %s:%d"% (options.host, options.port)
    tornado.ioloop.IOLoop.current().start()
